//
//  JsonModel.swift
//  testJSON
//
//  Created by AngryGOsha on 06.07.2021.
//

import Foundation


struct JsonData: Decodable {
    let id: String
    let name: String
    let type: String
    let dateOpened: String
    let dateClosed: String?
    let address: AdressData
    let phone: String
    let status: String
    let products: [String]
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case type = "type"
        case dateOpened = "date_opened"
        case dateClosed = "date_closed"
        case address = "address"
        case phone = "phone"
        case status = "status"
        case products = "products"
    }
}

struct AdressData: Decodable {
    let additionalInformation: String?
    let zip: String?
    let country: String
    let region: String
    let area: String?
    let city: String
    let town: String?
    let street: String
    let house: String?
    let myCase: String?
    let apartment: String?
    let gpsLat: Double
    let gpsLng: Double
    
    private enum CodingKeys: String, CodingKey {
        case additionalInformation = "additional_information"
        case zip = "zip"
        case country = "country"
        case region = "region"
        case area = "area"
        case city = "city"
        case town = "town"
        case street = "street"
        case house = "house"
        case myCase = "case"
        case apartment = "apartment"
        case gpsLat = "gps_lat"
        case gpsLng = "gps_lng"
    }
}
