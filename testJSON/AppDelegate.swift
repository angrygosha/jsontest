//
//  AppDelegate.swift
//  testJSON
//
//  Created by AngryGOsha on 06.07.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow()
        window?.rootViewController = ModuleBuilder().build()
        window?.makeKeyAndVisible()
        
        return true
    }


}

