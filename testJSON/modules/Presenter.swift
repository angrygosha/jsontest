//
//  Presenter.swift
//  testJSON
//
//  Created by AngryGOsha on 06.07.2021.
//

import Foundation

protocol PresenterProtocol: AnyObject {
    func loadData()
}

final class Presenter: PresenterProtocol {
    
    weak var viewController: ViewControllerProtocol?
    
    func loadData() {
        loadJson(urlString: "https://dl.dropboxusercontent.com/s/f3x990vmoxp5gis/test.json", completion: { result in
            DispatchQueue.main.async {
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                case .success(let data):
                    let decodedData: JsonData = try! JSONDecoder().decode(JsonData.self, from: data)
                    self.viewController?.applyData(decodedData)
                }
            }
            
        })
    }
    
    private func loadJson(urlString: String,
                          completion: @escaping(Result<Data, Error>) -> Void) {
        if let url = URL(string: urlString) {
            let urlSession = URLSession(configuration: .default).dataTask(with: url) { (data, _, error) in
                if let error = error {
                    completion(.failure(error))
                }
                
                if let data = data {
                    completion(.success(data))
                }
            }
            
            urlSession.resume()
        }
    }
}
