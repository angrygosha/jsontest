//
//  ViewController.swift
//  testJSON
//
//  Created by AngryGOsha on 06.07.2021.
//

import UIKit
import SnapKit

protocol ViewControllerProtocol: AnyObject {
    func applyData(_ data: JsonData)
}

class ViewController: UIViewController, ViewControllerProtocol {
    
    var presenter: PresenterProtocol
    
    let idLabel: UILabel = {
        let label = UILabel()
        label.text = "Id: "
        label.textAlignment = .center
        return label
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Name: "
        label.textAlignment = .center
        return label
    }()
    
    let townLabel: UILabel = {
        let label = UILabel()
        label.text = "Town: "
        label.textAlignment = .center
        return label
    }()
    
    let gpsLatLabel: UILabel = {
        let label = UILabel()
        label.text = "GpsLat: "
        label.textAlignment = .center
        return label
    }()
    
    let gpsLngLabel: UILabel = {
        let label = UILabel()
        label.text = "GpsLng: "
        label.textAlignment = .center
        return label
    }()
    
    let statusLabel: UILabel = {
        let label = UILabel()
        label.text = "Status: "
        label.textAlignment = .center
        return label
    }()
    
    lazy var snapView: UIStackView = {
        let stackV = UIStackView(arrangedSubviews: [idLabel,
                                                    nameLabel,
                                                    townLabel,
                                                    gpsLatLabel,
                                                    gpsLngLabel,
                                                    statusLabel])
        stackV.translatesAutoresizingMaskIntoConstraints = false
        stackV.axis = .vertical
        stackV.spacing = 10
        stackV.distribution = .equalSpacing
        return stackV
    }()
    
    init(presenter: PresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
        addSubviews()
        configureSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        view.addSubview(snapView)
    }
    
    private func configureSubviews() {
        snapView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        presenter.loadData()
    }
    
    func applyData(_ data: JsonData) {
        idLabel.text?.append(data.id)
        nameLabel.text?.append(data.name)
        townLabel.text?.append(data.address.town ?? "-")
        gpsLatLabel.text?.append("\(data.address.gpsLat)")
        gpsLngLabel.text?.append("\(data.address.gpsLng)")
        statusLabel.text?.append(data.status)
    }

}

