//
//  ModuleBuilder.swift
//  testJSON
//
//  Created by AngryGOsha on 06.07.2021.
//

import Foundation

final class ModuleBuilder {
    
    func build() -> ViewController {
        let presenter = Presenter()
        let viewController = ViewController(presenter: presenter)
        presenter.viewController = viewController
        return viewController
    }
    
}
